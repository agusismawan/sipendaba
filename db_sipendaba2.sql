-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Des 2019 pada 03.37
-- Versi Server: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sipendaba2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE IF NOT EXISTS `akses` (
  `status` int(11) NOT NULL,
  `level_akses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`status`, `level_akses`) VALUES
(1, 'Administrator'),
(2, 'Gudang'),
(3, 'Pimpinan'),
(4, 'Satgas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(40) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `nama_barang`, `id_kategori`, `stok`, `id_satuan`, `date_added`) VALUES
(35, 'S19A', 'LED Monitor Samsung S19A100N', 19, 3, 6, '2019-10-14 04:43:47'),
(37, 'NC-32', 'Cooling Pad Laptop Ace NC-32', 20, 1, 8, '2019-10-14 04:44:32'),
(42, '097855067081', 'Keyboard Logitech K120', 17, 0, 2, '2019-06-25 07:08:36'),
(43, 'KBLL01', 'Kabel LAN 1,5 M', 21, 0, 9, '2019-06-23 18:31:44'),
(44, 'KBLV01', 'Kabel VGA 1,5 M', 21, 0, 9, '2019-06-23 17:22:44'),
(45, 'BLU01', 'Tinta Blueprint Black 100 ml', 22, 1, 10, '2019-06-23 18:00:35'),
(46, 'BLU02', 'Tinta Blue Print Cyan 100 ml', 22, 0, 10, '2019-06-22 04:09:26'),
(47, 'HDD01', 'Harddisk WD 500GB Refurbish', 23, 0, 13, '2019-06-23 18:04:30'),
(48, 'HDD02', 'Harddisk Laptop 500GB new', 23, 1, 14, '2018-11-30 04:35:26'),
(49, '4547808804615', 'FLashdisk Toshiba 8GB Putih', 24, 7, 15, '2019-06-25 10:59:30'),
(50, '4047999400110', 'FLashdisk Toshiba 16GB Putih', 24, 5, 15, '2019-06-25 10:59:33'),
(51, '619659066093', 'Sandisk Cruzer Edge 8GB', 24, 1, 16, '2019-06-24 13:49:49'),
(52, '4712702617290', 'Silicon Power 8GB', 24, 9, 17, '2018-12-13 15:42:43'),
(53, '619659149598', 'SanDisk Dual Drive m30', 24, 6, 16, '2019-10-14 07:30:48'),
(54, '740617226751', 'RAM PC Kingston DDR 3 2GB', 25, 3, 18, '2019-06-23 16:53:20'),
(55, 'gld', 'RAM PC V-GEN DDR4 4 GB', 25, 2, 19, '2018-12-10 12:25:26'),
(56, '097855083104', 'Logitech Wireless M187', 16, 0, 6, '2019-10-10 11:02:25'),
(57, '4026203907911', 'TOSHIBA Optical Mouse U20', 16, 5, 15, '2019-10-14 07:29:16'),
(59, '8885007020341', 'EPSON Yellow T673', 22, 7, 11, '2019-06-22 08:17:11'),
(60, '8885007020310', 'EPSON Black T6731', 22, 1, 11, '2019-06-24 09:36:45'),
(61, '8885007020327', 'EPSON Cyan T6732', 22, 3, 11, '2019-06-25 14:36:04'),
(62, '4960999617053', 'Cartridge Canon 810 Black', 22, 5, 12, '2018-12-13 16:00:22'),
(63, '4960999617077', 'Cartridge Canon 811 Color', 22, 5, 12, '2018-12-10 13:00:15'),
(65, 'M1202', 'Keyboard Logitech K120c', 17, 0, 2, '2019-10-11 12:52:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_keluar`
--

CREATE TABLE IF NOT EXISTS `barang_keluar` (
  `id_barangkeluar` int(11) NOT NULL,
  `tgl_barangkeluar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kode_gudang` int(11) NOT NULL,
  `total_barang` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `no_kk` int(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barangkeluar`, `tgl_barangkeluar`, `kode_gudang`, `total_barang`, `no_invoice`, `no_kk`) VALUES
(20, '2018-11-28 22:25:34', 2, '130000', '28/RK/2/18/11/25/34', 0),
(21, '2018-11-30 04:19:45', 2, '515000', '30/RK/2/18/05/19/45', 0),
(22, '2018-11-30 04:24:10', 2, '50000', '30/RK/2/18/05/24/10', 0),
(23, '2018-11-30 04:35:42', 2, '700000', '30/RK/2/18/05/35/42', 0),
(24, '2018-11-30 04:36:35', 2, '70000', '30/RK/2/18/05/36/35', 0),
(25, '2018-11-30 04:37:09', 2, '270000', '30/RK/2/18/05/37/09', 0),
(26, '2018-11-30 05:06:37', 2, '249000', '30/RK/2/18/06/06/37', 0),
(27, '2018-11-30 05:07:09', 2, '85000', '30/RK/2/18/06/07/09', 0),
(28, '2018-11-30 05:07:39', 2, '75000', '30/RK/2/18/06/07/39', 0),
(29, '2018-12-22 01:07:00', 2, '140000', '22/RK/2/18/02/07/00', 0),
(30, '2018-12-22 01:08:02', 2, '203000', '22/RK/2/18/02/08/02', 0),
(31, '2018-12-27 05:13:26', 2, '470000', '27/RK/2/18/06/13/26', 0),
(32, '2018-12-27 05:15:44', 2, '422000', '27/RK/2/18/06/15/44', 0),
(33, '2019-02-06 08:58:16', 2, '345000', '06/RK/2/19/09/58/16', 0),
(34, '2019-05-28 22:36:52', 2, '305000', '29/RK/2/19/12/36/52', 0),
(37, '2019-06-22 08:18:08', 2, '', '22/RK/2/19/10/18/08', 0),
(38, '2019-06-23 16:53:24', 2, '', '23/RK/2/19/06/53/24', 0),
(39, '2019-06-23 17:22:52', 2, '3', '23/RK/2/19/07/22/52', 0),
(40, '2019-06-23 18:05:06', 2, '8', '23/RK/2/19/08/05/06', 0),
(41, '2019-06-23 18:15:00', 2, '3', '23/RK/2/19/08/15/00', 0),
(42, '2019-06-23 18:31:51', 2, '4', '23/RK/2/19/08/31/51', 0),
(44, '2019-06-24 11:48:48', 2, '1', '24/RK/2/19/01/48/48', 0),
(45, '2019-06-24 13:13:29', 2, '1', '24/RK/2/19/03/13/29', 12345678),
(46, '2019-06-24 13:15:01', 2, '2', '24/RK/2/19/03/15/01', 123456),
(47, '2019-06-24 13:50:58', 2, '9', '24/RK/2/19/03/50/58', 2147483647),
(48, '2019-06-25 04:21:54', 2, '1', '25/RK/2/19/06/21/54', 2147483647),
(49, '2019-06-25 07:09:19', 2, '1', '25/RK/2/19/09/09/19', 2147483647),
(50, '2019-06-25 14:36:08', 2, '1', '25/RK/2/19/04/36/08', 12),
(51, '2019-06-25 14:37:05', 2, '1', '25/RK/2/19/04/37/05', 12),
(70, '2019-10-11 12:39:36', 3, '5', '11/RK/3/19/02/39/36', 2147483647),
(71, '2019-10-11 12:52:43', 3, '10', '11/RK/3/19/02/52/43', 2147483647),
(72, '2019-10-14 04:12:55', 3, '17', '14/RK/3/19/06/12/55', 0),
(73, '2019-10-14 07:30:59', 3, '3', '14/RK/3/19/09/30/59', 2147483647),
(74, '2019-10-14 07:39:35', 3, '10', '14/RK/3/19/09/39/35', 2147483647);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_permintaan`
--

CREATE TABLE IF NOT EXISTS `detail_permintaan` (
  `dp_id` int(11) NOT NULL,
  `permintaan_id` varchar(50) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `dp_jumlah` int(11) NOT NULL,
  `permintaan_status` enum('diterima','ditolak','menunggu') DEFAULT 'menunggu'
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_permintaan`
--

INSERT INTO `detail_permintaan` (`dp_id`, `permintaan_id`, `kode_barang`, `dp_jumlah`, `permintaan_status`) VALUES
(16, 'REQ-1812140830', 'S19A', 3, 'menunggu'),
(17, 'REQ-1812140830', 'KBLL01', 5, 'menunggu'),
(18, 'REQ-1812141132', 'S19A', 5, 'diterima'),
(19, 'REQ-1812141132', 'KBLL01', 10, 'diterima'),
(20, 'REQ-1812141132', 'HDD02', 2, 'menunggu'),
(23, 'REQ-1905081205', 'BLU01', 1, 'menunggu'),
(24, 'REQ-1905081205', 'BLU02', 0, 'ditolak'),
(25, 'REQ-1905081205', '8885007020341', 10, 'diterima'),
(26, 'REQ-1905240003', 'S19A', 1, 'diterima'),
(27, 'REQ-1905240003', 'NC-32', 1, 'ditolak'),
(28, 'REQ-1906191853', 'S19A', 5, 'menunggu'),
(31, 'REQ-1906201102', 'S19A', 10, 'menunggu'),
(32, 'REQ-1906201116', 'KBLL01', 5, 'menunggu'),
(33, 'REQ-1906201209', 'S19A', 5, 'diterima'),
(34, 'REQ-1906201221', 'S19A', 5, 'diterima'),
(35, 'REQ-1906201221', 'KBLL01', 5, 'diterima'),
(36, 'REQ-1906201221', 'KBLV01', 5, 'menunggu'),
(37, 'REQ-1906201221', 'BLU01', 5, 'menunggu'),
(38, 'REQ-1906201221', 'HDD01', 5, 'menunggu'),
(39, 'REQ-1906201221', 'HDD02', 5, 'menunggu'),
(43, 'REQ-1906201303', '097855083104', 5, 'menunggu'),
(44, 'REQ-1906201303', '4026203907911', 5, 'menunggu'),
(45, 'REQ-1906242043', '097855083104', 5, 'menunggu'),
(46, 'REQ-1906242043', '4026203907911', 2, 'menunggu'),
(47, 'REQ-1906242043', '619659066093', 2, 'menunggu'),
(48, 'REQ-1906242349', '097855083104', 2, 'menunggu'),
(49, 'REQ-1906242349', '4026203907911', 2, 'menunggu'),
(50, 'REQ-1906251043', '097855083104', 1, 'menunggu'),
(51, 'REQ-1906251043', '4026203907911', 2, 'diterima'),
(52, 'REQ-1906251106', '619659149598', 3, 'diterima'),
(53, 'REQ-1906251106', '740617226751', 0, 'ditolak'),
(57, 'REQ-1906251339', '097855083104', 5, 'diterima'),
(58, 'REQ-1906261150', '4026203907911', 5, 'diterima'),
(59, 'REQ-1906261150', 'M1202', 5, 'diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(16, 'Mouse'),
(17, 'Keyboard'),
(19, 'Monitor'),
(20, 'Cooling Pad'),
(21, 'Kabel'),
(22, 'Tinta Print'),
(23, 'Harddisk'),
(24, 'Flashdisk'),
(25, 'RAM'),
(26, 'Printer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permintaan`
--

CREATE TABLE IF NOT EXISTS `permintaan` (
  `permintaan_id` varchar(50) NOT NULL,
  `permintaan_tgl` date NOT NULL,
  `no_kk` int(50) NOT NULL,
  `permintaan_nama` varchar(128) NOT NULL,
  `permintaan_alamat` text NOT NULL,
  `permintaan_ket` text,
  `archived` int(1) NOT NULL DEFAULT '0',
  `status_notif` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `permintaan`
--

INSERT INTO `permintaan` (`permintaan_id`, `permintaan_tgl`, `no_kk`, `permintaan_nama`, `permintaan_alamat`, `permintaan_ket`, `archived`, `status_notif`) VALUES
('REQ-1812140830', '2018-12-14', 0, '', '', 'Segera dibaca', 0, 1),
('REQ-1812141132', '2018-12-15', 0, '', '', 'Urgebt', 0, 0),
('REQ-1905081205', '2019-05-08', 0, '', '', 'Cepat', 0, 0),
('REQ-1905240003', '2019-05-24', 0, '', '', 'SIWI', 0, 0),
('REQ-1906191853', '2019-06-19', 0, '', '', 'qwert', 0, 0),
('REQ-1906201102', '2019-06-20', 0, '', '', 'baca', 0, 0),
('REQ-1906201116', '2019-06-22', 123, 'asd', 'asd', 'asd', 0, 0),
('REQ-1906201209', '2019-06-24', 12345678, 'ani', 'kudus', 'gawat', 0, 0),
('REQ-1906201221', '2019-06-26', 1234, 'ags', 'jpr', 'cpt', 0, 0),
('REQ-1906201303', '2019-06-27', 123456, 'add', 'add', 'add', 0, 0),
('REQ-1906242043', '2019-06-30', 2147483647, 'Achmad Saerozi', 'Gunungwungkal Pati', 'Bencana Alam', 0, 0),
('REQ-1906242349', '2019-06-25', 12, 'galeh', 'ae', 'ngeleh', 0, 0),
('REQ-1906251043', '2019-06-25', 123, 'riko', 'kudus', 'bantuan', 0, 0),
('REQ-1906251106', '2019-06-25', 2147483647, 'Riski Manunggal', 'Jekulo', 'Patah Hati', 0, 0),
('REQ-1906251339', '2019-06-25', 2147483647, 'Erwin Fahrul', 'Srikandang', 'Banjir', 0, 0),
('REQ-1906261150', '2019-06-26', 2147483647, 'Fredo Maulana Putra', 'Jekulo Kudus', 'Longsor', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`) VALUES
(1, 'Advance'),
(2, 'Logitech'),
(3, 'Sonicgear'),
(4, 'Dazumba'),
(5, 'Simbadda'),
(6, 'Samsung'),
(7, 'Dell'),
(8, 'ACE'),
(9, 'Izinet'),
(10, 'Blue Print'),
(11, 'Epson'),
(12, 'Canon'),
(13, 'Western Digital'),
(14, 'Seagate'),
(15, 'Toshiba'),
(16, 'SanDisk'),
(17, 'Silicon Power'),
(18, 'Kingston'),
(19, 'V-GEN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_barangkeluar`
--

CREATE TABLE IF NOT EXISTS `sub_barangkeluar` (
  `id_subbarangkeluar` int(11) NOT NULL,
  `kode_barang` varchar(40) NOT NULL,
  `id_barangkeluar` int(11) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `no_invoice` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_barangkeluar`
--

INSERT INTO `sub_barangkeluar` (`id_subbarangkeluar`, `kode_barang`, `id_barangkeluar`, `jumlah_keluar`, `no_invoice`) VALUES
(1, '097855083104', 70, 5, '11/RK/3/19/02/39/36'),
(2, '4026203907911', 71, 5, '11/RK/3/19/02/52/43'),
(3, 'M1202', 71, 5, '11/RK/3/19/02/52/43'),
(4, 'S19A', 72, 5, '14/RK/3/19/06/12/55'),
(5, 'KBLL01', 72, 10, '14/RK/3/19/06/12/55'),
(6, 'HDD02', 72, 2, '14/RK/3/19/06/12/55'),
(7, '619659149598', 73, 3, '14/RK/3/19/09/30/59'),
(8, '740617226751', 73, 0, '14/RK/3/19/09/30/59'),
(9, '4026203907911', 74, 5, '14/RK/3/19/09/39/35'),
(10, 'M1202', 74, 5, '14/RK/3/19/09/39/35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tempo`
--

CREATE TABLE IF NOT EXISTS `tempo` (
  `id_subbarangkeluar` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `trx` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tempo`
--

INSERT INTO `tempo` (`id_subbarangkeluar`, `id_barang`, `jumlah_keluar`, `trx`) VALUES
(2, 35, 2, '10/RK/2/19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `alamat`, `notlp`, `status`, `date_created`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', '', '', 1, '2019-06-20 07:14:58'),
(2, 'gudang', 'a80dd043eb5b682b4148b9fc2b0feabb2c606fff', 'Avoni Putri', 'Kudus', '085641345768', 2, '2019-06-26 04:57:10'),
(3, 'pimpinan', '59335c9f58c78597ff73f6706c6c8fa278e08b3a', 'Hutomo Rusdianto', 'Kudus', '085641888999', 3, '2018-11-19 06:29:23'),
(4, 'satgas', '0117cbfa57c75b6ac6e01fde98a44501ebc15d75', 'Satgas', 'Kudus', '089211000777', 4, '2018-11-20 13:37:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`status`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD UNIQUE KEY `kode_barang` (`kode_barang`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_merk` (`id_satuan`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `kode_barang_2` (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barangkeluar`),
  ADD KEY `id_transaksi` (`id_barangkeluar`);

--
-- Indexes for table `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  ADD PRIMARY KEY (`dp_id`),
  ADD KEY `permintaan_id` (`permintaan_id`),
  ADD KEY `permintaan_id_2` (`permintaan_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`permintaan_id`),
  ADD KEY `permintaan_id` (`permintaan_id`),
  ADD KEY `permintaan_id_2` (`permintaan_id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`),
  ADD KEY `id_merk` (`id_satuan`);

--
-- Indexes for table `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  ADD PRIMARY KEY (`id_subbarangkeluar`),
  ADD KEY `id_transaksi` (`id_barangkeluar`),
  ADD KEY `id_barang` (`kode_barang`);

--
-- Indexes for table `tempo`
--
ALTER TABLE `tempo`
  ADD PRIMARY KEY (`id_subbarangkeluar`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  MODIFY `dp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  MODIFY `id_subbarangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tempo`
--
ALTER TABLE `tempo`
  MODIFY `id_subbarangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id_satuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  ADD CONSTRAINT `detail_permintaan_ibfk_3` FOREIGN KEY (`permintaan_id`) REFERENCES `permintaan` (`permintaan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  ADD CONSTRAINT `sub_barangkeluar_ibfk_1` FOREIGN KEY (`id_barangkeluar`) REFERENCES `barang_keluar` (`id_barangkeluar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_barangkeluar_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`status`) REFERENCES `akses` (`status`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<script type="text/javascript">
	<?php
	if ($_SESSION['status'] == 1) {
		?>
		document.title = "Detail laporan";
		document.getElementById('laporan').classList.add('active');
	<?php
	} else {
		?>
		document.title = "Detail transaksi";
		document.getElementById('transaksi').classList.add('active');
	<?php } ?>
</script>

<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<?php
				if ($_SESSION['status'] == 1) {
					?>
					<h3 class="jdl">Detail Laporan</h3>
				<?php } else { ?>
					<h3 class="jdl">Detail Transaksi</h3>
				<?php } ?>
				<?php
				$getqheader = $root->con->query("select permintaan.*, barang_keluar.* from barang_keluar inner join permintaan on permintaan.no_kk=barang_keluar.no_kk where id_barangkeluar='$_GET[id_barangkeluar]'");
				$getqheader = $getqheader->fetch_assoc();
				?>
				<table width="30%">
					<tr>
						<td><span class="label">Nama Penerima</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= $getqheader['permintaan_nama'] ?></span></td>
					</tr>
					<tr>
						<td><span class="label">Nomor KK</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= $getqheader['no_kk'] ?></span></td>
					</tr>
					<tr>
						<td><span class="label">Alamat</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= $getqheader['permintaan_alamat'] ?></span></td>
					</tr>
					<tr>
						<td><span class="label">Keterangan</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= $getqheader['permintaan_ket'] ?></span></td>
					</tr>
					<tr>
						<td><span class="label">Tanggal Barang Keluar</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= date("d-m-Y", strtotime($getqheader['tgl_barangkeluar'])) ?></span></td>
					</tr>
					<tr>
						<td><span class="label">No Invoice</span></td>
						<td><span class="label">:</span></td>
						<td><span class="label"><?= $getqheader['no_invoice'] ?></span></td>
					</tr>
				</table>
				<table class="datatable" style="width: 100%;">
					<thead>
						<tr>
							<th width="35px">NO</th>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
							<th>Jumlah</th>
							<th>Satuan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$trx = date("d") . "/AF/" . $_SESSION['status'] . "/" . date("y");
						$data = $root->con->query("select barang.kode_barang,barang.nama_barang,sub_barangkeluar.jumlah_keluar,satuan.nama_satuan from sub_barangkeluar inner join barang on barang.kode_barang=sub_barangkeluar.kode_barang inner join satuan on satuan.id_satuan=barang.id_satuan where sub_barangkeluar.id_barangkeluar='$_GET[id_barangkeluar]' and sub_barangkeluar.jumlah_keluar > 0");
						$getsum = $root->con->query("select sum(jumlah_keluar) as jumlah_keluar from sub_barangkeluar where id_barangkeluar='$_GET[id_barangkeluar]'");
						$getsum1 = $getsum->fetch_assoc();
						$no = 1;
						while ($f = $data->fetch_assoc()) {
							?><tr>
								<td><?= $no++ ?></td>
								<td><?= $f['kode_barang'] ?></td>
								<td><?= $f['nama_barang'] ?></td>
								<td><?= $f['jumlah_keluar'] ?></td>
								<td><?= $f['nama_satuan'] ?></td>
							</tr>
						<?php
						}
						?>
						<tr>
							<td></td>
							<td></td>
							<td>Total Barang Keluar :</td>
							<td><?= $getsum1['jumlah_keluar'] ?></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<br>
				<div class="left">
					<?php
					$link = ($_SESSION['status'] == 3) ? "laporan.php" : "laporan.php";
					?>
					<a href="<?= $link ?>" class="btnblue" style="background: #f33155"><i class="fa fa-mail-reply"></i> Kembali</a>
					<?php if ($_SESSION['status'] == 2) {
						?>
						<a href="cetak_nota.php?oid=<?= base64_encode($_GET['id_barangkeluar']) ?>&id-uid=<?= base64_encode($getqheader['nama_pembeli']) ?>&inf=<?= base64_encode($getqheader['no_invoice']) ?>&tb=<?= base64_encode($f['total_bayar']) ?>&uuid=<?= base64_encode(date("d-m-Y", strtotime($getqheader['tgl_barangkeluar']))) ?>" class="btnblue" target="_blank"><i class="fa fa-print"></i> Cetak </a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
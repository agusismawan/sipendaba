<script type="text/javascript">
	document.title = "Detail transaksi";
	document.getElementById('transaksi').classList.add('active');

	$('.disabled').click(function(e) {
		e.preventDefault();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		if ($('#contenh').clone().children().remove().end().text() == "menunggu") {
			$('#prosestran').attr("disabled", "disabled");
			$('#prosestran').attr("title", "konfirmasi barang terlebih dahulu");
			$('#prosestran').css("background", "#ccc");
			$('#prosestran').css("cursor", "not-allowed");
		}
	})
</script>

<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<?php
				if ($_SESSION['status'] == 1) {
				?>
					<h3 class="jdl">Detail Permintaan</h3>
				<?php } else { ?>
					<h3 class="jdl">Detail Permintaan</h3>
				<?php } ?>

				<form class="form-input" action="handler.php?action=selesai_transaksi" method="post">
					<?php
					$getqheader = $root->con->query("select * from permintaan where permintaan_id='$_GET[permintaan_id]'");
					$getqheader = $getqheader->fetch_assoc();
					?>
					<table width="50%" style="border: 0">
						<tr>
							<td><span class="label">Kode Permintaan</span></td>
							<td><span class="label">: </span></td>
							<td>
								<span class="label"><?= $getqheader['permintaan_id'] ?></span>
								<input type="hidden" name="permintaan_id" value="<?= $getqheader['permintaan_id'] ?>">
							</td>
						</tr>
						<tr>
							<td><span class="label">Tanggal Permintaan</span></td>
							<td><span class="label">: </span></td>
							<td><span class="label"><?= date("d-m-Y", strtotime($getqheader['permintaan_tgl'])) ?></span></td>
						</tr>
						<tr>
							<td><span class="label">Nomor KK</span></td>
							<td><span class="label">: </span></td>
							<td>
								<!-- <span class="label"><?= $getqheader['no_kk'] ?></span> -->
								<input type="text" name="no_kk" value="<?= $getqheader['no_kk'] ?>" readonly>
							</td>
						</tr>
						<tr>
							<td><span class="label">Nama</span></td>
							<td><span class="label">: </span></td>
							<td><span class="label"><?= $getqheader['permintaan_nama'] ?></span></td>
						</tr>
						<tr>
							<td><span class="label">Alamat</span></td>
							<td><span class="label">: </span></td>
							<td><span class="label"><?= $getqheader['permintaan_alamat'] ?></span></td>
						</tr>
						<tr>
							<td><span class="label">Keterangan</span></td>
							<td><span class="label">: </span></td>
							<td><span class="label"><?= $getqheader['permintaan_ket'] ?></span></td>
						</tr>
					</table>
					<table class="datatable" style="width: 100%;">
						<thead>
							<tr>
								<th width="35px">NO</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Stok Barang</th>
								<th>Permintaan</th>
								<th>Status</th>
								<?php
								if ($_SESSION['status'] == 3) {
								?>
									<th width="100px">Aksi</th>
								<?php } ?>
							</tr>

						</thead>
						<tbody id="contenth">
							<?php
							$trx = date("d") . "/AF/" . $_SESSION['status'] . "/" . date("y");
							$data = $root->con->query("select barang.kode_barang, barang.nama_barang, barang.stok, barang.tgl_kadaluarsa,detail_permintaan.dp_id, detail_permintaan.dp_jumlah, detail_permintaan.permintaan_status from detail_permintaan inner join barang on barang.kode_barang=detail_permintaan.kode_barang where detail_permintaan.permintaan_id='$_GET[permintaan_id]'");
							$getsum = $root->con->query("select sum(dp_jumlah) as jumlah_permintaan from detail_permintaan where permintaan_id='$_GET[permintaan_id]'");
							$getsum1 = $getsum->fetch_assoc();
							$no = 1;
							while ($f = $data->fetch_assoc()) {
							?><tr>
									<td><?= $no++ ?></td>
									<td>
										<?= $f['kode_barang'] ?>
										<input type="hidden" name="kode_barang" value="<?= $f['kode_barang'] ?>">
									</td>
									<td>
										<?= $f['nama_barang'] ?> <?php if ($f['tgl_kadaluarsa'] != null) {
																		echo "[$f[tgl_kadaluarsa]]";
																	} ?>
									</td>
									<td><?= $f['stok'] ?></td>
									<td>
										<?= $f['dp_jumlah'] ?>
										<input type="hidden" name="jumlah_keluar" value="<?= $f['dp_jumlah'] ?>">
									</td>
									<td>
										<?php
										if ($f['permintaan_status'] == 'menunggu') {
											echo "<span class='badge badge-warning'>$f[permintaan_status]</span>";
										} elseif ($f['permintaan_status'] == 'diterima') {
											echo "<span class='badge badge-success'>$f[permintaan_status]</span>";
										} else {
											echo "<span class='badge badge-danger'>$f[permintaan_status]</span>";
										}
										?>
									</td>
									<td>
										<?php if ($_SESSION['status'] == 3) { ?>
											<?php if ($f['permintaan_status'] == 'menunggu') { ?>
												<!-- terima -->
												<a class="btnblue" href="?action=diterima_detail_permintaan&dp_id=<?= $f['dp_id'] ?>&permintaan_id=<?= $_GET['permintaan_id'] ?>&kode_barang=<?= $f['kode_barang'] ?>&dp_jumlah=<?= $f['dp_jumlah'] ?>"><i class="fa fa-check"></i></a>

												<!-- tolak -->
												<a class="btnblue" style="background: #f33155" href="?action=ditolak_detail_permintaan&dp_id=<?= $f['dp_id'] ?>&permintaan_id=<?= $_GET['permintaan_id'] ?>""><i class=" fa fa-times"></i></a>
											<?php } elseif ($f['permintaan_status'] == 'ditolak') { ?>
												<!-- <a class="btnblue" href="?action=diterima_detail_permintaan&dp_id=<?= $f['dp_id'] ?>&permintaan_id=<?= $_GET['permintaan_id'] ?>"><i class="fa fa-check"></i></a> -->
											<?php } else { ?>
												<!-- <a class="btnblue" style="background: #f33155" href="?action=ditolak_detail_permintaan&dp_id=<?= $f['dp_id'] ?>&permintaan_id=<?= $_GET['permintaan_id'] ?>""><i class=" fa fa-times"></i></a> -->
											<?php } ?>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td>Total Permintaan Barang :</td>
								<td>
									<?= $getsum1['jumlah_permintaan'] ?>
									<input type="hidden" name="jumlah_permintaan" value="<?= $getsum1['jumlah_permintaan'] ?>">
								</td>

								<td></td>
							</tr>
						</tbody>
					</table>
					<br>
					<div class="left">
						<a href="list_permintaan.php" class="btnblue" style="background: #f33155"><i class="fa fa-mail-reply"></i> Kembali</a>
						<!-- <button class='btn btn-primary' id='prosestran' type='submit'><i class='fa fa-save'></i> Simpan</button> -->
						<?php if ($_SESSION['status'] == 3 || $_SESSION['status'] == 2) { ?>
							<button class="btn btn-primary" id="prosestran" type="submit"><i class="fa fa-save"></i> Proses</button>
						<?php } ?>

						<?php
						$data = $root->con->query("select barang.kode_barang, barang.nama_barang, barang.stok, detail_permintaan.dp_id, detail_permintaan.dp_jumlah, detail_permintaan.permintaan_status from detail_permintaan inner join barang on barang.kode_barang=detail_permintaan.kode_barang where detail_permintaan.permintaan_id='$_GET[permintaan_id]'");
						// while ($key = $data->fetch_assoc()) {
						// 	if ($key['permintaan_status'] == 'menunggu') {
						// 		echo "<button class='btn btn-secondary' id='prosestran' type='submit' disabled><i class='fa fa-save'></i> Simpan</button>";
						// 	} else {
						// 		echo "<button class='btn btn-primary' id='prosestran' type='submit' disabled><i class='fa fa-save'></i> Simpan</button>";
						// 	}
						// }
						?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
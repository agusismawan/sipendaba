<?php

require_once('assets/lib/tcpdf/tcpdf.php');

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Laporan Barang Keluar');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(10, 10, 10, true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 1);

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('Times', '', 10);

function fetch_data()
{
    // Check connection
    $output = '';
    mysql_connect("localhost", "root", "");
    mysql_select_db("db_sipendaba2");
    $tanggal = $_POST['tgl_laporan'];
    if ($_POST['jenis_laporan'] == "perhari") {
        $split1 = explode('-', $tanggal);
        $tanggal = $split1[2] . "-" . $split1[1] . "-" . $split1[0];
        $query = mysql_query("SELECT barang_keluar.*, permintaan.* FROM barang_keluar INNER JOIN permintaan
        ON permintaan.no_kk=barang_keluar.no_kk where barang_keluar.tgl_barangkeluar like '%$tanggal%' GROUP BY barang_keluar.id_barangkeluar ORDER BY barang_keluar.id_barangkeluar DESC");
    } elseif($_POST['jenis_laporan'] == "perbulan") {
        $split1 = explode('-', $tanggal);
        $tanggal = $split1[1] . "-" . $split1[0];
        $query = mysql_query("SELECT barang_keluar.*, permintaan.* FROM barang_keluar INNER JOIN permintaan
        ON permintaan.no_kk=barang_keluar.no_kk where barang_keluar.tgl_barangkeluar like '%$tanggal%' GROUP BY barang_keluar.id_barangkeluar ORDER BY barang_keluar.id_barangkeluar DESC");
    } else {
        $split1=explode('-',$tanggal);
        $tanggal=$split1[0];
        $query=mysql_query("SELECT barang_keluar.*, permintaan.* FROM barang_keluar INNER JOIN permintaan
        ON permintaan.no_kk=barang_keluar.no_kk where barang_keluar.tgl_barangkeluar like '%$tanggal%' GROUP BY barang_keluar.id_barangkeluar ORDER BY barang_keluar.id_barangkeluar DESC");
    }

    // output data of each row
    while ($row = mysql_fetch_array($query)) {

        $output .= '<tr>
    			<td>' . $row['no_invoice'] . '</td>
    			<td>' . $row['permintaan_nama'] . '</td>
    			<td>' . $row['no_kk'] . '</td>
    			<td>' . $row['permintaan_alamat'] . '</td>
    			<td>' . $row['permintaan_ket'] . '</td>
                <td><table>';

        $query2 = mysql_query("select barang.kode_barang,barang.nama_barang,sub_barangkeluar.jumlah_keluar,satuan.nama_satuan from sub_barangkeluar inner join barang on barang.kode_barang=sub_barangkeluar.kode_barang inner join satuan on satuan.id_satuan=barang.id_satuan where sub_barangkeluar.id_barangkeluar='$row[id_barangkeluar]'");
        while ($q = mysql_fetch_array($query2)) {
            $output .= '<tr><td>- ' . $q['nama_barang'] . ' : ' . $q['jumlah_keluar'] . '' . $q['nama_satuan'] . '</td></tr>';
        }
        $output .= '</table></td><td align="center">' . $row['total_barang'] . '</td>
                    <td>' . date("d-m-Y H:i:s", strtotime($row['tgl_barangkeluar'])) . '</td>
    			</tr>';
    }
    return $output;
}


$content  = '';
$content .= '
    <table style="text-align: center" border="0px">
    <tr>
        <th rowspan="6" width="115px"><img src="assets/img/logo.png" width="80px"></th>
        <th width="70%"><strong>REKAPITULASI BARANG KELUAR</strong></th>
    </tr>
    <tr>
        <td style="font-size:12"><strong>BADAN PENANGGULANGAN BENCANA DAERAH KABUPATEN JEPARA</strong></td>
    </tr>
    <tr>
        <td>Jl. Mangunsaskoro No. 41 Jepara Telp. (0291)598216</td>
    </tr>
    <tr>
        <td>Website : www.bpbdkabjepara.go.id Email : bpbdkabjepara@gmail.com</td>
    </tr>
    <tr>
        <td><strong>Data Laporan ' . $_POST['tgl_laporan'] . '</strong></td>
    </tr>
    <tr>
        <td><strong>Jenis : ' . $_POST['jenis_laporan'] . '</strong></td>
    </tr>
    </table>

    <table style="text-align:right">
        <tr>
            <td>Jepara, ' . date("d F Y") . '</td>
        </tr>
    </table>
    <br>
    <br>
    <table border="1px" style="padding:5px">  
    <tr>
        <th align="center"><b>No Invoice</b></th>
        <th align="center"><b>Nama Penerima</b></th>
        <th align="center"><b>Nomor KK</b></th>
        <th align="center"><b>Alamat</b></th>
        <th align="center"><b>Keterangan</b></th>
        <th align="center" width="145px"><b>Detail Barang</b></th>
        <th align="center" width="45px"><b>Total</b></th>
        <th align="center"><b>Tanggal Barang Keluar</b></th>
    </tr>';

$content .= fetch_data();
$content .= '
</table>';

// output the HTML content
$pdf->writeHTML($content, true, true, true, true, '');

// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('laporan_barang_keluar.pdf', 'I');

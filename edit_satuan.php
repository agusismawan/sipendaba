<script type="text/javascript">
	document.title="Edit List Satuan";
	document.getElementById('satuan').classList.add('active');
</script>

<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Edit Satuan</h3>
				<?php $f=$root->edit_satuan($_GET['id_satuan']) ?>
				<form class="form-input" method="post" action="handler.php?action=edit_satuan">
					<input type="text" placeholder="ID Satuan" disabled="disabled" value="ID satuan : <?= $f['id_satuan'] ?>">
					<input type="text" name="nama_satuan" placeholder="Nama Barang" required="required" value="<?= $f['nama_satuan'] ?>">
					<input type="hidden" name="id_satuan" value="<?= $f['id_satuan'] ?>">
					<button class="btnblue" type="submit"><i class="fa fa-save"></i> Update</button>
					<a href="satuan.php" class="btnblue" style="background: #f33155"><i class="fa fa-close"></i> Batal</a>
				</form>
			</div>
		</div>
	</div>
</div>

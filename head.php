<?php
include "root.php";
session_start();
if (!isset($_SESSION['username'])) {
	$root->redirect("index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="assets/index.css">
	<link rel="stylesheet" type="text/css" href="assets/awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="assets/jquery/jquery.js"></script>

</head>

<body>

	<div class="sidebar">
		<h3><i class="fa fa fa-desktop"></i> &nbsp;BPBD Jepara</h3>
		<ul><?php
			if ($_SESSION['status'] == 1) {
				?>
				<li class="admin-info">
					<img src="assets/img/admin.png">
					<span><?php echo $_SESSION['username']; ?> </span>
				</li>
				<li><a id="dash" href="home.php"><i class="fa fa-home"></i> Dashboard</a></li>
				<li><a id="barang" href="barang.php"><i class="fa fa-table"></i> Barang</a></li>
				<li><a id="kategori" href="kategori.php"><i class="fa fa-bullseye"></i> List Kategori</a></li>
				<li><a id="satuan" href="satuan.php"><i class="fa fa-bullseye"></i> List Satuan</a></li>
				<li><a id="tambah_permintaan" href="tambah_permintaan.php"><i class='fa fa-upload'></i></i> Permintaan Bantuan</a></li>
				<li><a id="list_permintaan" href="list_permintaan.php"><i class='fa fa-bullseye'></i></i> List Permintaan</a></li>
				<li><a id="users" href="users.php"><i class="fa fa-users"></i> Kelola User</a></li>
				<li><a id="laporan" href="laporan.php"><i class="fa fa-book"></i> Laporan</a></li>
				<li>

				<?php
				} else if ($_SESSION['status'] == 2) {
					?>
				<li class="admin-info">
					<img src="assets/img/admin.png">
					<span><?php echo $_SESSION['username']; ?></span>
				</li>
				<!-- <li><a id="transaksi" href="transaksi.php"><i class="fa fa-money"></i> Transaksi Barang Keluar</a></li> -->
				<!-- <li><a id="list_permintaan" href="list_permintaan.php"><i class='fa fa-bullseye'></i></i> List Permintaan</a></li> -->
				<li><a id="barang" href="barang.php"><i class="fa fa-table"></i> Barang</a></li>
				<li><a id="kategori" href="kategori.php"><i class="fa fa-bullseye"></i> List Kategori</a></li>
				<li><a id="satuan" href="satuan.php"><i class="fa fa-bullseye"></i> List Satuan</a></li>
			<?php
			} else if ($_SESSION['status'] == 3) {
				?>
				<li class="admin-info">
					<img src="assets/img/manager.png">
					<span><?php echo $_SESSION['username']; ?></span>
				</li>
				<li><a id="barang" href="barang.php"><i class="fa fa-table"></i> Barang</a></li>
				<li><a id="list_permintaan" href="list_permintaan.php"><i class='fa fa-list'></i></i> List Permintaan</a></li>
				<li><a id="laporan" href="laporan.php"><i class="fa fa-book"></i> Laporan</a></li>

			<?php
			} else if ($_SESSION['status'] == 4) {
				?>
				<li class="admin-info">
					<img src="assets/img/admin.png">
					<span><?php echo $_SESSION['username']; ?></span>
				</li>
				<li><a id="dash" href="home.php"><i class="fa fa-home"></i> Dashboard</a></li>
				<li><a id="tambah_permintaan" href="tambah_permintaan.php"><i class='fa fa-upload'></i></i> Permintaan Bantuan</a></li>
				<li><a id="list_permintaan" href="list_permintaan.php"><i class='fa fa-bullseye'></i></i> List Permintaan</a></li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="nav">
		<ul>
			<li><a href=""><i class="fa fa-user"></i> Hai , <?= $_SESSION['nama'] ?>!</a>
				<ul>
					<?php
					if ($_SESSION['status'] == 1) {
						?>
						<li><a href="setting_akun.php"><i class="fa fa-cog"></i> Pengaturan Akun</a></li>
					<?php } ?>
					<li><a href="handler.php?action=logout"><i class="fa fa-sign-out"></i> Logout</a></li>
				</ul>
			</li>
			<li>
				<a href=""><i class="fa fa-bell"></i>
					<span class="badge badge-danger badge-counter jumlah"></span>
				</a>
				<ul>
					<div class="infoNotif">

					</div>
				</ul>
			</li>
		</ul>
	</div>

	<script>
		var base_url = "http://localhost:8080/sipendaba2/";

		function cek() {
			$.ajax({
				url: base_url + "root.php/getJumlah",
				cache: false,
				dataType: 'json',
				success: function(msg) {
					console.log(msg.jumlah)
					if (msg.jumlah == 0) {
						$('.jumlah').hide()
						$(".infoNotif").html(msg.kosong);
					} else {
						$(".jumlah").show().html(msg.jumlah);
						$(".infoNotif").html(msg.kosong);
					}
				}
			});
			var waktu = setTimeout("cek()", 2000);
		}
	</script>
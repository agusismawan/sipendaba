<?php
require('assets/lib/fpdf.php');
class PDF extends FPDF
{
    function Header()
    {
        $this->SetFont('Arial','B',14);
        $this->Cell(30,10,'Badan Penanggulangan Bencana Daerah Kabupaten Jepara');

        $this->Ln(5);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'Jl. Mangunsaskoro No. 41 Jepara Telp. (0291)598216');


        $this->Ln(5);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'Website : www.bpbdkabjepara.go.id Email : bpbdkabjepara@gmail.com');


        $this->Ln(8);
        $this->SetFont('Arial','B',10);
        $this->cell(30,10,'Data Laporan Tanggal : '.$_POST['tgl_laporan'].'');

        $this->Ln(5);
        $this->SetFont('Arial','B',10);
        $this->cell(30,10,'Jenis : '.$_POST['jenis_laporan'].'');

        $this->cell(130);
        $this->SetFont('Arial','',10);
        $this->cell(30,10,'Jepara, '.date("d-m-Y").'');

        $this->Line(10,45,200,45);
    }
    function data_barang(){
        mysql_connect("localhost","root","");
        mysql_select_db("db_sipendaba2");
        $tanggal=$_POST['tgl_laporan'];
        if ($_POST['jenis_laporan']=="perhari") {
            $split1=explode('-',$tanggal);
            $tanggal=$split1[2]."-".$split1[1]."-".$split1[0];
            $query=mysql_query("select barang_keluar.id_barangkeluar,barang_keluar.tgl_barangkeluar,barang_keluar.no_invoice,barang_keluar.total_barang,barang_keluar.no_kk,user.username from barang_keluar inner join user on barang_keluar.kode_gudang=user.id where barang_keluar.tgl_barangkeluar like '%$tanggal%' order by barang_keluar.id_barangkeluar desc");
        }else{
            $split1=explode('-',$tanggal);
            $tanggal=$split1[1]."-".$split1[0];
            $query=mysql_query("select barang_keluar.id_barangkeluar,barang_keluar.tgl_barangkeluar,barang_keluar.no_invoice,barang_keluar.total_barang,barang_keluar.no_kk,user.username from barang_keluar inner join user on barang_keluar.kode_gudang=user.id where barang_keluar.tgl_barangkeluar like '%$tanggal%' order by barang_keluar.id_barangkeluar desc");
        }
        while ($r=  mysql_fetch_array($query))
                {
                    $hasil[]=$r;
                }
                return $hasil;
                
    }
    function set_table($data){
        $this->SetFont('Arial','B',9);
        $this->Cell(10,7,"No",1);
        $this->Cell(40,7,"No Invoice",1);
        $this->Cell(20,7,"Gudang",1);
        $this->Cell(40,7,"Nomor KK",1);
        $this->Cell(40,7,"Tanggal Barang Keluar",1);
        $this->Cell(40,7,"Total Barang",1);
        $this->Ln();

        $this->SetFont('Arial','',9);
        $no=1;
        foreach($data as $row)
        {
            $this->Cell(10,7,$no++,1);
            $this->Cell(40,7,$row['no_invoice'],1);
            $this->Cell(20,7,$row['username'],1);
            $this->Cell(40,7,$row['no_kk'],1);
            $this->Cell(40,7,date("d-m-Y h:i:s",strtotime($row['tgl_barangkeluar'])),1);
            $this->Cell(40,7,$row['total_barang'],1);
            $this->Ln();
        }
    }
}

$pdf = new PDF();
$pdf->SetTitle('Cetak Data Barang');

$data = $pdf->data_barang();

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(20);
$pdf->set_table($data);
$pdf->Output('','BPBD/B-OUT/'.date("d-m-Y").'.pdf');

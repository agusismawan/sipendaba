-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2019 at 07:51 
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sipendaba2`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses`
--

CREATE TABLE IF NOT EXISTS `akses` (
  `status` int(11) NOT NULL,
  `level_akses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses`
--

INSERT INTO `akses` (`status`, `level_akses`) VALUES
(1, 'Administrator'),
(2, 'Gudang'),
(3, 'Pimpinan'),
(4, 'Satgas');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(40) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `nama_barang`, `id_kategori`, `stok`, `id_satuan`, `date_added`) VALUES
(35, 'S19A', 'LED Monitor Samsung S19A100N', 19, 1, 6, '2019-12-10 04:21:16'),
(37, 'NC-32', 'Cooling Pad Laptop Ace NC-32', 20, 1, 8, '2019-10-14 04:44:32'),
(42, '097855067081', 'Keyboard Logitech K120', 17, 0, 2, '2019-06-25 07:08:36'),
(43, 'KBLL01', 'Kabel LAN 1,5 M', 21, 0, 9, '2019-06-23 18:31:44'),
(44, 'KBLV01', 'Kabel VGA 1,5 M', 21, 0, 9, '2019-06-23 17:22:44'),
(45, 'BLU01', 'Tinta Blueprint Black 100 ml', 22, 1, 10, '2019-06-23 18:00:35'),
(46, 'BLU02', 'Tinta Blue Print Cyan 100 ml', 22, 0, 10, '2019-06-22 04:09:26'),
(47, 'HDD01', 'Harddisk WD 500GB Refurbish', 23, 0, 13, '2019-06-23 18:04:30'),
(48, 'HDD02', 'Harddisk Laptop 500GB new', 23, 1, 14, '2018-11-30 04:35:26'),
(49, '4547808804615', 'FLashdisk Toshiba 8GB Putih', 24, 7, 15, '2019-06-25 10:59:30'),
(50, '4047999400110', 'FLashdisk Toshiba 16GB Putih', 24, 5, 15, '2019-06-25 10:59:33'),
(51, '619659066093', 'Sandisk Cruzer Edge 8GB', 24, 1, 16, '2019-06-24 13:49:49'),
(52, '4712702617290', 'Silicon Power 8GB', 24, 9, 17, '2018-12-13 15:42:43'),
(53, '619659149598', 'SanDisk Dual Drive m30', 24, 6, 16, '2019-10-14 07:30:48'),
(54, '740617226751', 'RAM PC Kingston DDR 3 2GB', 25, 3, 18, '2019-06-23 16:53:20'),
(55, 'gld', 'RAM PC V-GEN DDR4 4 GB', 25, 2, 19, '2018-12-10 12:25:26'),
(59, '8885007020341', 'EPSON Yellow T673', 22, 2, 11, '2019-12-10 04:43:40'),
(60, '8885007020310', 'EPSON Black T6731', 22, 1, 11, '2019-06-24 09:36:45'),
(61, '8885007020327', 'EPSON Cyan T6732', 22, 3, 11, '2019-06-25 14:36:04'),
(62, '4960999617053', 'Cartridge Canon 810 Black', 22, 5, 12, '2018-12-13 16:00:22'),
(63, '4960999617077', 'Cartridge Canon 811 Color', 22, 5, 12, '2018-12-10 13:00:15'),
(65, 'M1202', 'Keyboard Logitech K120c', 17, 0, 2, '2019-10-11 12:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE IF NOT EXISTS `barang_keluar` (
  `id_barangkeluar` int(11) NOT NULL,
  `tgl_barangkeluar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kode_gudang` int(11) NOT NULL,
  `total_barang` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `no_kk` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_permintaan`
--

CREATE TABLE IF NOT EXISTS `detail_permintaan` (
  `dp_id` int(11) NOT NULL,
  `permintaan_id` varchar(50) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `dp_jumlah` int(11) NOT NULL,
  `permintaan_status` enum('diterima','ditolak','menunggu') DEFAULT 'menunggu'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_permintaan`
--

INSERT INTO `detail_permintaan` (`dp_id`, `permintaan_id`, `kode_barang`, `dp_jumlah`, `permintaan_status`) VALUES
(3, 'REQ-1912101142', '8885007020341', 3, 'diterima');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(16, 'Mouse'),
(17, 'Keyboard'),
(19, 'Monitor'),
(20, 'Cooling Pad'),
(21, 'Kabel'),
(22, 'Tinta Print'),
(23, 'Harddisk'),
(24, 'Flashdisk'),
(25, 'RAM'),
(26, 'Printer');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE IF NOT EXISTS `permintaan` (
  `permintaan_id` varchar(50) NOT NULL,
  `permintaan_tgl` date NOT NULL,
  `no_kk` varchar(128) NOT NULL,
  `permintaan_nama` varchar(128) NOT NULL,
  `permintaan_alamat` text NOT NULL,
  `permintaan_ket` text,
  `archived` int(1) NOT NULL DEFAULT '0',
  `status_notif` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permintaan`
--

INSERT INTO `permintaan` (`permintaan_id`, `permintaan_tgl`, `no_kk`, `permintaan_nama`, `permintaan_alamat`, `permintaan_ket`, `archived`, `status_notif`) VALUES
('REQ-1912101142', '2019-12-10', '34050101778538', 'Indina', 'Jakarta', 'Bencana angin puting beliung', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`) VALUES
(1, 'Advance'),
(2, 'Logitech'),
(3, 'Sonicgear'),
(4, 'Dazumba'),
(5, 'Simbadda'),
(6, 'Samsung'),
(7, 'Dell'),
(8, 'ACE'),
(9, 'Izinet'),
(10, 'Blue Print'),
(11, 'Epson'),
(12, 'Canon'),
(13, 'Western Digital'),
(14, 'Seagate'),
(15, 'Toshiba'),
(16, 'SanDisk'),
(17, 'Silicon Power'),
(18, 'Kingston'),
(19, 'V-GEN');

-- --------------------------------------------------------

--
-- Table structure for table `sub_barangkeluar`
--

CREATE TABLE IF NOT EXISTS `sub_barangkeluar` (
  `id_subbarangkeluar` int(11) NOT NULL,
  `kode_barang` varchar(40) NOT NULL,
  `id_barangkeluar` int(11) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `no_invoice` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tempo`
--

CREATE TABLE IF NOT EXISTS `tempo` (
  `id_subbarangkeluar` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `trx` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempo`
--

INSERT INTO `tempo` (`id_subbarangkeluar`, `id_barang`, `jumlah_keluar`, `trx`) VALUES
(2, 35, 2, '10/RK/2/19');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `alamat`, `notlp`, `status`, `date_created`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', '', '', 1, '2019-06-20 07:14:58'),
(2, 'gudang', 'a80dd043eb5b682b4148b9fc2b0feabb2c606fff', 'Avoni Putri', 'Kudus', '085641345768', 2, '2019-06-26 04:57:10'),
(3, 'pimpinan', '59335c9f58c78597ff73f6706c6c8fa278e08b3a', 'Hutomo Rusdianto', 'Kudus', '085641888999', 3, '2018-11-19 06:29:23'),
(4, 'satgas', '0117cbfa57c75b6ac6e01fde98a44501ebc15d75', 'Satgas', 'Kudus', '089211000777', 4, '2018-11-20 13:37:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`status`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD UNIQUE KEY `kode_barang` (`kode_barang`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_merk` (`id_satuan`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `kode_barang_2` (`kode_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barangkeluar`),
  ADD KEY `id_transaksi` (`id_barangkeluar`),
  ADD KEY `no_kk` (`no_kk`);

--
-- Indexes for table `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  ADD PRIMARY KEY (`dp_id`),
  ADD KEY `permintaan_id` (`permintaan_id`),
  ADD KEY `permintaan_id_2` (`permintaan_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`permintaan_id`),
  ADD KEY `permintaan_id` (`permintaan_id`),
  ADD KEY `permintaan_id_2` (`permintaan_id`),
  ADD KEY `no_kk` (`no_kk`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`),
  ADD KEY `id_merk` (`id_satuan`);

--
-- Indexes for table `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  ADD PRIMARY KEY (`id_subbarangkeluar`),
  ADD KEY `id_transaksi` (`id_barangkeluar`),
  ADD KEY `id_barang` (`kode_barang`);

--
-- Indexes for table `tempo`
--
ALTER TABLE `tempo`
  ADD PRIMARY KEY (`id_subbarangkeluar`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  MODIFY `dp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  MODIFY `id_subbarangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tempo`
--
ALTER TABLE `tempo`
  MODIFY `id_subbarangkeluar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id_satuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_permintaan`
--
ALTER TABLE `detail_permintaan`
  ADD CONSTRAINT `detail_permintaan_ibfk_3` FOREIGN KEY (`permintaan_id`) REFERENCES `permintaan` (`permintaan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_barangkeluar`
--
ALTER TABLE `sub_barangkeluar`
  ADD CONSTRAINT `sub_barangkeluar_ibfk_1` FOREIGN KEY (`id_barangkeluar`) REFERENCES `barang_keluar` (`id_barangkeluar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_barangkeluar_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `barang` (`kode_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`status`) REFERENCES `akses` (`status`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

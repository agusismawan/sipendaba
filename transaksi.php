<?php include "head.php" ?>
<?php
if (isset($_GET['action']) && $_GET['action'] == "transaksi_baru") {
	include "transaksi_baru.php";
} else if (isset($_GET['action']) && $_GET['action'] == "detail_transaksi") {
	include "detail_transaksi.php";
} else {
	?>
	<script type="text/javascript">
		document.title = "Transaksi";
		document.getElementById('transaksi').classList.add('active');
	</script>
	<div class="content">
		<div class="padding">
			<div class="bgwhite">
				<div class="padding">
					<div class="contenttop">
						<div class="left">
							<a href="?action=transaksi_baru" class="btnblue">Transaksi Baru</a>
						</div>
						<div class="both"></div>
					</div>
					<span class="label">Jumlah Transaksi : <?= $root->show_jumlah_trans() ?></span>
					<table class="datatable">
						<thead>
							<tr>
								<th width="35px">NO</th>
								<th>Tanggal Barang Keluar</th>
								<th>Total Barang</th>
								<th>Nomor KK</th>
								<th>No Invoice</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 1;
								$q = $root->con->query("select * from barang_keluar where kode_gudang='$_SESSION[id]' order by barang_keluar.id_barangkeluar desc");
								if ($q->num_rows > 0) {
									while ($f = $q->fetch_assoc()) {
										?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= date("d-m-Y", strtotime($f['tgl_barangkeluar'])) ?></td>
										<td><?= $f['total_barang'] ?></td>
										<td><?= $f['no_kk'] ?>
										<td><?= $f['no_invoice'] ?></td>
										<td>
											<a href="?action=detail_transaksi&id_barangkeluar=<?= $f['id_barangkeluar'] ?>" class="btn bluetbl m-r-10"><span class="btn-edit-tooltip">Detail</span><i class="fa fa-eye"></i></a>
											<a href="cetak_nota.php?oid=<?= base64_encode($f['id_barangkeluar']) ?>&id-uid=<?= base64_encode($f['no_kk']) ?>&inf=<?= base64_encode($f['no_invoice']) ?>&tb=<?= base64_encode($f['total_bayar']) ?>&uuid=<?= base64_encode(date("d-m-Y", strtotime($f['tgl_barangkeluar']))) ?>" target="_blank" class="btn bluetbl"><span class="btn-hapus-tooltip">Cetak</span><i class="fa fa-print"></i></a>
										</td>
									</tr>
								<?php
										}
									} else {
										?>
								<td><?= $no++ ?></td>
								<td colspan="5">Belum Ada Transaksi</td>
							<?php
								}
								?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php
}
include "foot.php" ?>
<?php

require_once('assets/lib/tcpdf/tcpdf.php');

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Laporan Data Barang');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(10, 10, 10, true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 1);

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('Times', '', 10);

function fetch_data()
{
	// Check connection
	$output = '';
	mysql_connect("localhost", "root", "");
	mysql_select_db("db_sipendaba2");
	$data = mysql_query("SELECT barang.id_barang,barang.kode_barang,barang.nama_barang,satuan.nama_satuan,barang.stok,barang.date_added,kategori.nama_kategori from barang INNER JOIN kategori on kategori.id_kategori=barang.id_kategori INNER JOIN satuan ON satuan.id_satuan=barang.id_satuan ORDER BY barang.id_barang DESC");

	// output data of each row
	$no = 1;
	while ($row = mysql_fetch_array($data)) {

		$output .= '<tr>
    			<td align="center">' . $no++ . '</td>
    			<td>' . $row['kode_barang'] . '</td>
    			<td>' . $row['nama_barang'] . '</td>
    			<td>' . $row['nama_kategori'] . '</td>
    			<td align="center">' . $row['stok'] . '</td>
    			<td>' . $row['nama_satuan'] . '</td>
    			<td>' . date("d-m-Y", strtotime($row['date_added'])) . '</td>
                </tr>';
	}
	return $output;
}


$content  = '';
$content .= '
    <table style="text-align: center" border="0px">
    <tr>
        <th rowspan="6" width="115px"><img src="assets/img/logo.png" width="80px"></th>
        <th width="70%"><strong>REKAPITULASI DATA BARANG</strong></th>
    </tr>
    <tr>
        <td style="font-size:12"><strong>BADAN PENANGGULANGAN BENCANA DAERAH</strong></td>
    </tr>
    <tr>
        <td style="font-size:12"><strong>KABUPATEN JEPARA</strong></td>
    </tr>
    <tr>
        <td>Jl. Mangunsaskoro No. 41 Jepara Telp. (0291)598216</td>
    </tr>
    <tr>
        <td>Website : www.bpbdkabjepara.go.id Email : bpbdkabjepara@gmail.com</td>
    </tr>
    </table>
	<br><br>
    <table style="text-align:right">
        <tr>
            <td>Jepara, ' . date("d F Y") . '</td>
        </tr>
    </table>
    <br>
    <br>
    <table border="1px" style="padding:5px">  
    <tr>
        <th align="center" width="25px"><b>No</b></th>
        <th align="center"><b>Kode Barang</b></th>
        <th align="center" width="150px"><b>Nama Barang</b></th>
        <th align="center"><b>Kategori</b></th>
        <th align="center" width="45px"><b>Stok</b></th>
        <th align="center"><b>Satuan</b></th>
        <th align="center"><b>Tanggal Ditambahkan</b></th>
    </tr>';

$content .= fetch_data();
$content .= '
</table>';

// output the HTML content
$pdf->writeHTML($content, true, true, true, true, '');

// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('laporan_data_barang.pdf', 'I');

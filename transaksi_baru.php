<script type="text/javascript">
	document.title = "Transaksi Baru";
	document.getElementById('transaksi').classList.add('active');
</script>
<script type="text/javascript">
	$(document).ready(function() {
		if ($.trim($('#contenth').text()) == "") {
			$('#prosestran').attr("disabled", "disabled");
			$('#prosestran').attr("title", "tambahkan barang terlebih dahulu");
			$('#prosestran').css("background", "#ccc");
			$('#prosestran').css("cursor", "not-allowed");
		}
	})
</script>
<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Entry Transaksi Baru</h3>
				<form class="form-input" method="post" action="handler.php?action=tambah_tempo" style="padding-top: 30px;">
					<table border="0" cellspacing="8">
						<thead>
							<tr>
								<td><label>Pilih Barang</label></td>
								<td><label>Jumlah</label></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select style="width: 372px;cursor: pointer;" required="required" name="id_barang">
										<?php
										$data = $root->con->query("select * from barang inner join satuan on satuan.id_satuan=barang.id_satuan");
										while ($f = $data->fetch_assoc()) {
											echo "<option value='$f[id_barang]'>$f[nama_barang] (stock : $f[stok] $f[nama_satuan] )</option>";
										}
										?>
									</select>
								</td>
								<td><input style="width: 50px" required="required" type="number" name="jumlah" placeholder="0"></td>
								<td><button class="btnblue" type="submit"><i class="fa fa-plus"></i> Tambahkan</button></td>
								<input type="hidden" name="trx" value="<?php echo date("d") . "/RK/" . $_SESSION['id'] . "/" . date("y") ?>">
							</tr>
						</tbody>
					</table>
				</form>

			</div>
		</div>
		<br>
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Data transaksi</h3>
				<table class="datatable" style="width: 100%;">
					<thead>
						<tr>
							<th width="35px">NO</th>
							<th>Kode Barang</th>
							<th>Nama Barang</th>
							<th>Jumlah</th>
							<th>Satuan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody id="contenth">
						<?php
						$trx = date("d") . "/RK/" . $_SESSION['id'] . "/" . date("y");
						$data = $root->con->query("select barang.kode_barang,barang.nama_barang,tempo.id_subbarangkeluar,tempo.id_barang,tempo.jumlah_keluar, satuan.nama_satuan from tempo inner join barang on barang.id_barang=tempo.id_barang inner join satuan on satuan.id_satuan=barang.id_satuan where trx='$trx'");
						$getsum = $root->con->query("select sum(jumlah_keluar) as grand_total from tempo where trx='$trx'");
						$getsum1 = $getsum->fetch_assoc();
						$no = 1;
						while ($f = $data->fetch_assoc()) {
							?><tr>
								<td><?= $no++ ?></td>
								<td><?= $f['kode_barang'] ?></td>
								<td><?= $f['nama_barang'] ?></td>
								<td><?= $f['jumlah_keluar'] ?></td>
								<td><?= $f['nama_satuan'] ?></td>
								<td><a href="handler.php?action=hapus_tempo&id_tempo=<?= $f['id_subbarangkeluar'] ?>&id_barang=<?= $f['id_barang'] ?>&jumbel=<?= $f['jumlah_keluar'] ?>" class="btn redtbl"><span class="btn-hapus-tooltip">Cancel</span><i class="fa fa-close"></i></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>

					<tr>
						<?php if ($getsum1['grand_total'] > 0) { ?>
							<td colspan="2"></td>
							<td>Total Barang :</td>
							<td><?= $getsum1['grand_total'] ?></td>
							<td></td>
						<?php } else { ?>
							<td colspan="6">Data masih kosong</td>
						<?php } ?>
					</tr>

				</table>
				<br>
				<form class="form-input" action="handler.php?action=selesai_transaksi" method="post">
					<!-- <label>Nama Pembeli :</label>
					<input style="width: 200px" type="text" name="nama_pembeli" value='Umum'> -->
					<select style="width: 372px; cursor: pointer;" required="required" name="no_kk">
						<option value="">Pilih Penerima :</option>
						<?php $root->tampil_nama(); ?>
					</select>
					<input type="hidden" name="total_barang" value="<?= $getsum1['grand_total'] ?>">
					<button class="btnblue" id="prosestran" type="submit"><i class="fa fa-save"></i> Proses Transaksi</button>
				</form>

			</div>
		</div>


	</div>
</div>

<?php
include "foot.php";
?>
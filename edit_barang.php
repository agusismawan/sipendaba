<script type="text/javascript">
	document.title = "Edit Barang";
	document.getElementById('barang').classList.add('active');
</script>

<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Edit Barang</h3>
				<?php
				$f = $root->edit_barang($_GET['id_barang']);
				?>
				<form class="form-input" method="post" action="handler.php?action=edit_barang" style="padding-top: 30px;"> <input type="hidden" name="id_barang" value="<?= $f['id_barang'] ?>">
					<input type="text" placeholder="ID Kategori" disabled="disabled" value="ID barang : <?= $f['id_barang'] ?>">
					<label>Kode Barang :</label>
					<input type="text" name="kode_barang" placeholder="Kode Barang" required="required" value="<?= $f['kode_barang'] ?>">
					<label>Nama Barang :</label>
					<input type="text" name="nama_barang" placeholder="Nama Barang" required="required" value="<?= $f['nama_barang'] ?>">
					<label>Kategori :</label>
					<select style="width: 372px;cursor: pointer;" required="required" name="kategori">
						<option value="">Pilih Kategori :</option>
						<?php $root->tampil_kategori3($_GET['id_barang']); ?>
					</select>
					<input type="date" name="tgl_kadaluarsa" class="tgl_kdl" value="<?= $f['tgl_kadaluarsa'] ?>">
					<small class="text-danger">*) Untuk produk <strong>makanan/minuman</strong> tanggal kadaluarsa wajib diisi!</small>
					<br><br>
					<label>Stok :</label>
					<input name="stok" placeholder="Stok" required="required" value="<?= $f['stok'] ?>">
					<label>Satuan :</label>
					<select style="width: 372px;cursor: pointer;" required="required" name="satuan">
						<option value="">Pilih Satuan :</option>
						<?php $root->tampil_satuan3($_GET['id_barang']); ?>
					</select>

					<button class="btnblue" type="submit"><i class="fa fa-save"></i> Simpan</button>
					<a href="barang.php" class="btnblue" style="background: #f33155"><i class="fa fa-close"></i> Batal</a>
				</form>
			</div>
		</div>
	</div>
</div>